﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using HHScreenManager.Classi;
using WebEye.Controls.Wpf;
using System.IO;

namespace HHScreenManager
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly string mClassName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name;

        RemoteWindow rw;
        string sourceselected ="image";
        WebCameraId wId;

        bool VideoSet = false;
        TimeSpan totaltime;


        //string Pdf_file;
        string Image_file;
        string Video_File;

        List<MyImage> listImage;



        public MainWindow()
        {
            InitializeComponent();
            InitializeComboCamera();
            InitializeListScreen();
        }

        private void InitializeComboCamera()
        {
            try
            {
                cmb_Camera.ItemsSource = we_Camera.GetVideoCaptureDevices();

                if (cmb_Camera.Items.Count > 0)
                {
                    cmb_Camera.SelectedItem = cmb_Camera.Items[0];
                    wId = (WebCameraId)cmb_Camera.SelectedItem; 
                }

                
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void InitializeListScreenOLD()
        {
            try
            {

                System.Windows.Forms.Screen[] lista = System.Windows.Forms.Screen.AllScreens;

                foreach (System.Windows.Forms.Screen s in lista)
                {
                    if (!s.Primary)
                    {
                        lb_Monitor.Items.Add(s.DeviceName);
                    }
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
           
        }

        private void InitializeListScreen()
        {
            try
            {
                lb_Monitor.ItemsSource = Monitors.GetMonitors(false);
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Tbtn_Image_Click(object sender, EventArgs e)
        {
            try
            {
                rd_Image.IsChecked = true;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Tbtn_Stream_Click(object sender, EventArgs e)
        {
            try
            {
                rd_Stream.IsChecked = true;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Tbtn_Video_Click(object sender, EventArgs e)
        {
            try
            {
                rd_Video.IsChecked = true;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Tbtn_Scrittura_Click(object sender, EventArgs e)
        {
            try
            {
                rd_Scrittura.IsChecked = true;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RD_Source_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Controls.RadioButton rd = (System.Windows.Controls.RadioButton)sender;
                string tipo = rd.Tag.ToString();
                if (rw != null)
                {
                    sourceselected = tipo;
                    rw.SelectType(tipo);
                }

            }
            catch(Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void Cmb_Camera_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                wId = (WebCameraId)cmb_Camera.SelectedItem;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Btn_Remote_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rw != null)
                {
                    rw.Close();
                    btn_Remote.Content = "Open Remote Window";
                    rw = null;
                }
                else
                {
                    

                    Monitor dis =(Monitor)lb_Monitor.SelectedItem;


                    if (dis != null)
                    {
                        
                        var lav = dis.screen.WorkingArea;

                        var lavbound = dis.screen.Bounds;

                        rw = new RemoteWindow();
                        rw.Loaded += Rw_Loaded;
                        rw.Top = lav.Top;
                        rw.Left = lav.Left;
                        rw.Width = lav.Width;
                        rw.Height = lavbound.Height;
                        rw.Topmost = true;

                        rw.Show();
                        rw.Closing += Rw_Closing;
                        btn_Remote.Content = "Close Remote Window";

                        if (Video_File != null)
                        {
                            rw.UpdateVideo(Video_File);
                        }

                        if (Image_file != null)
                        {
                            rw.UpdateImage(new BitmapImage(new Uri(Image_file)));
                        }

                        //if (Pdf_file != null)
                        //{
                        //    rw.UpdatePDF(Pdf_file);
                        //}
                    }
                    else
                    {
                        MessageBox.Show("Attenzione non è stato selezionato nessun monitor sul quale aprire la finestra remota", "Attenzione", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }


                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);                               
            }


        }

        private void Btn_Remote_ClickOLD(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rw != null)
                {
                    rw.Close();
                    btn_Remote.Content = "Open Remote Window";
                    rw = null;
                }
                else
                {
                    System.Windows.Forms.Screen[] lista = System.Windows.Forms.Screen.AllScreens;

                    var dis = lb_Monitor.SelectedValue;


                    if (dis != null)
                    {
                        System.Windows.Forms.Screen sel = lista.Where(t => t.DeviceName.ToString() == dis.ToString()).First();

                        var lav = sel.WorkingArea;

                        var lavbound = sel.Bounds;

                        rw = new RemoteWindow();
                        rw.Loaded += Rw_Loaded;
                        rw.Top = lav.Top;
                        rw.Left = lav.Left;
                        rw.Width = lav.Width;
                        rw.Height = lavbound.Height;
                        rw.Topmost = true;

                        rw.Show();
                        rw.Closing += Rw_Closing;
                        btn_Remote.Content = "Close Remote Window";

                        if (Video_File != null)
                        {
                            rw.UpdateVideo(Video_File);
                        }

                        if (Image_file != null)
                        {
                            rw.UpdateImage(new BitmapImage(new Uri(Image_file)));
                        }

                        //if (Pdf_file != null)
                        //{
                        //    rw.UpdatePDF(Pdf_file);
                        //}
                    }
                    else
                    {
                        MessageBox.Show("Attenzione non è stato selezionato nessun monitor sul quale aprire la finestra remota", "Attenzione", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }


                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void Rw_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                btn_StopStream.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                btn_StopVideo.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Rw_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                rw.WindowState = WindowState.Maximized;
                //rw.SelectType(sourceselected);

            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }


        //#region PDF
        //private void Btn_Pdf_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (Pdf_file == null)
        //        {
        //            System.Windows.Forms.OpenFileDialog op = new System.Windows.Forms.OpenFileDialog();
        //            op.Title = "Select a picture";
        //            op.Filter = "PDF Files|*.pdf";
        //            var x = op.ShowDialog();

        //            if (x == System.Windows.Forms.DialogResult.OK)
        //            {
        //                pdf_Viewer.LoadFile(op.FileName);
        //                Pdf_file = op.FileName;
        //                if (rw != null)
        //                {
        //                    rw.UpdatePDF(Pdf_file);
        //                    btn_Pdf.Content = "UnSet PDF";
        //                }

        //            }
        //        }
        //        else
        //        {
        //            Pdf_file = null;
        //            btn_Pdf.Content = "Set PDF";
        //            pdf_Viewer = new WPFPdfViewer.PdfViewer();
        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
        //        MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }

        //}

        //#endregion

        #region Video
        private void Btn_Video_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog op = new System.Windows.Forms.OpenFileDialog();
                op.Title = "Select a picture";
                op.Filter = "All supported graphics|*.wmv;*.avi;*.mp4|" +
                  "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                  "Portable Network Graphic (*.png)|*.png";
                var x = op.ShowDialog();

                if (x == System.Windows.Forms.DialogResult.OK)
                {
                    Video_File = op.FileName;
                    if (rw != null)
                    {
                        rw.UpdateVideo(Video_File);
                        rw.TickTimer += Rw_TickTimer;
                        rw.EndVideo += Rw_EndVideo;
                        VideoSet = true;
                        btn_PlayVideo.IsEnabled = true;
                        sl_Timer.IsEnabled = true;
                        txbl_NomeFileVideo.Text = "Nome File: " + op.SafeFileName;
                    }

                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void Rw_EndVideo(object sender, EndVideoEventArgs e)
        {
            try
            {
                if (e.VideoEnd)
                {
                    if (ckb_TermineVideo.IsChecked == true)
                    {
                        if (rb_PassaScrittura.IsChecked == true)
                        {
                            rd_Scrittura.IsChecked = true;
                        }
                        if(rb_PassaStream.IsChecked== true)
                        {
                            rd_Stream.IsChecked = true;
                        }
                        if (rb_PassaChiudi.IsChecked== true)
                        {
                            rw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Rw_TickTimer(object sender, TickTimerEventArgs e)
        {
            try
            {
                totaltime = e.TotalTime;
                lbl_Timer.Content= String.Format("{0:hh}:{0:mm}:{0:ss}/{1:hh}:{1:mm}:{1:ss}", e.NowTime, e.TotalTime);
                if (Double.IsNaN(e.NowTime.TotalSeconds * 100 / e.TotalTime.TotalSeconds)== false)
                {
                    sl_Timer.Value = e.NowTime.TotalSeconds * 100 / e.TotalTime.TotalSeconds;
                }
                
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        

        private void Btn_PlayVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(rw != null && VideoSet)
                {
                    btn_PlayVideo.IsEnabled = false;
                    btn_PauseVideo.IsEnabled = true;
                    btn_StopVideo.IsEnabled = true;
                    rw.Play_Video();
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Btn_Pause_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rw != null && VideoSet)
                {
                    btn_PlayVideo.IsEnabled = true;
                    btn_PauseVideo.IsEnabled = false;
                    btn_StopVideo.IsEnabled = true;
                    rw.Pause_Video();
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Btn_Stop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rw != null && VideoSet)
                {
                    btn_PlayVideo.IsEnabled = true;
                    btn_PauseVideo.IsEnabled = false;
                    btn_StopVideo.IsEnabled = false;
                    rw.Stop_Video();
                    sl_Timer.Value = 0;
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Sl_Timer_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            try
            {
                if (rw != null && VideoSet)
                {
                    if (totaltime.TotalSeconds > 0)
                    {
                        rw.VideoPosition = TimeSpan.FromSeconds(sl_Timer.Value * totaltime.TotalSeconds / 100);
                        btn_PlayVideo.IsEnabled = true;
                        btn_PauseVideo.IsEnabled = false;
                        btn_StopVideo.IsEnabled = true;
                        rw.Play_Video();
                        rw.Pause_Video();
                    }
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Sl_Timer_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            try
            {
                if (rw != null && VideoSet)
                {
                    rw.Pause_Video();
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Sl_Timer_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            try
            {
                if (rw != null && VideoSet)
                {
                    rw.VideoPosition = TimeSpan.FromSeconds(sl_Timer.Value * totaltime.TotalSeconds / 100);
                    lbl_Timer.Content = String.Format("{0:hh}:{0:mm}:{0:ss}/{1:hh}:{1:mm}:{1:ss}", rw.VideoPosition, totaltime);
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region Image

        private void Btn_Image_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Image_file == null)
                {
                    System.Windows.Forms.OpenFileDialog op = new System.Windows.Forms.OpenFileDialog();

                    op.Title = "Select a picture";
                    op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                      "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                      "Portable Network Graphic (*.png)|*.png";
                    op.Multiselect = true;
                    var x = op.ShowDialog();

                    if (x == System.Windows.Forms.DialogResult.OK)
                    {
                        listImage = new List<MyImage>();

                        foreach (var f in op.FileNames)
                        {
                            FileInfo file = new FileInfo(f);

                            listImage.Add(new MyImage
                            {
                                Name = file.Name,
                                ImageData = new BitmapImage(new Uri(f)),
                                PathFile=f
                            });
                        }
                        lv_immagini.ItemsSource = listImage;
                        Image_file = op.FileName;
                    }

                    txbl_btn_Image.Text = "Rimuovi Selezione Immagini";
                }
                else
                {
                    Image_file = null;
                    listImage.Clear();
                    lv_immagini.ItemsSource = null;
                    txbl_btn_Image.Text = "Seleziona Immagini";
                    //img_Image.Source = null;
                    if (rw!= null)
                    {
                        rw.UpdateImage(null);
                    }
                    
                }

                //if (Image_file == null)
                //{
                //    System.Windows.Forms.OpenFileDialog op = new System.Windows.Forms.OpenFileDialog();

                //    op.Title = "Select a picture";
                //    op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                //      "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                //      "Portable Network Graphic (*.png)|*.png";
                //    op.Multiselect = true;
                //    var x = op.ShowDialog();

                //    if (x == System.Windows.Forms.DialogResult.OK)
                //    {
                //        img_Image.Source = new BitmapImage(new Uri(op.FileName));
                //        Image_file = op.FileName;
                //        if (rw != null)
                //        {
                //            rw.UpdateImage(new BitmapImage(new Uri(Image_file)));
                //        }
                //    }

                //    txbl_btn_Image.Text = "Rimuovi Selezione Immagine";
                //}
                //else
                //{
                //    Image_file = null;
                //    txbl_btn_Image.Text = "Seleziona Immagine";
                //    img_Image.Source = null;
                //    rw.UpdateImage(null);
                //}

            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Lv_immagini_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (rw != null)
                {
                    if (lv_immagini.SelectedItem != null)
                    {
                        MyImage SelectorImage = (MyImage)lv_immagini.SelectedItem;
                        Image_file = SelectorImage.PathFile;

                        rw.UpdateImage(new BitmapImage(new Uri(Image_file)));
                    }

                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region Stream
        private void Btn_StartStream_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (wId != null)
                {
                    if (rw != null)
                    {
                        btn_StartStream.IsEnabled = false;
                        btn_StopStream.IsEnabled = true;
                        rw.StartStream(wId);
                    }
                }                
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Btn_StopStream_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rw != null && wId != null)
                {
                    btn_StartStream.IsEnabled = true;
                    btn_StopStream.IsEnabled = false;
                    rw.Stopstream();
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }





        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var result = MessageBox.Show("Sicuro di voler usciere?", "Info", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result== MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void Btn_AudioTest_Click(object sender, RoutedEventArgs e)
        {
            AudioWindowTest at = new AudioWindowTest();
            at.Show();
        }


    }
}
