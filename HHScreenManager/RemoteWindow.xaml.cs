﻿
using HHScreenManager.Classi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WebEye.Controls.Wpf;

namespace HHScreenManager
{
    /// <summary>
    /// Logica di interazione per RemoteWindow.xaml
    /// </summary>
    public partial class RemoteWindow : Window
    {
        private static readonly string mClassName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name;

        WebCameraId web_id;
        DispatcherTimer seeker;
        TimeSpan totaltime;

        public event EventHandler<TickTimerEventArgs> TickTimer;

        public event EventHandler<EndVideoEventArgs> EndVideo;

        

        public RemoteWindow()
        {
            InitializeComponent();           
        }

        public RemoteWindow( string tipo)
        {
            InitializeComponent();
            SelectType(tipo);
        }

        public void SelectType(string tipo)
        {
            try
            {
                switch (tipo)
                {
                    case "image":
                        //row_Image.Height = GridLength.Auto;
                        //row_Video.Height = new GridLength(0);
                        ////row_PDF.Height = new GridLength(0);
                        //row_Stream.Height = new GridLength(0);
                        //row_Scrittura.Height = new GridLength(0);
                        img_Image.Visibility = Visibility.Visible;
                        me_Video.Visibility = Visibility.Hidden;
                        gr_Stream.Visibility = Visibility.Hidden;
                        img_Scrittura.Visibility = Visibility.Hidden;

                        img_Image.Height = this.ActualHeight -13;
                        img_Image.Width = this.ActualWidth - 13;

                        break;

                    case "video":
                        //row_Image.Height = new GridLength(0);
                        //row_Video.Height = GridLength.Auto;
                        ////row_PDF.Height = new GridLength(0);
                        //row_Stream.Height = new GridLength(0);
                        //row_Scrittura.Height = new GridLength(0);
                        img_Image.Visibility = Visibility.Hidden;
                        me_Video.Visibility = Visibility.Visible;
                        gr_Stream.Visibility = Visibility.Hidden;
                        img_Scrittura.Visibility = Visibility.Hidden;

                        break;

                    //case "pdf":
                    //    row_Image.Height = new GridLength(0);
                    //    row_Video.Height = new GridLength(0);
                    //    row_PDF.Height = GridLength.Auto;
                    //    row_Stream.Height = new GridLength(0);
                    //    row_Scrittura.Height= new GridLength(0);
                    //    break;

                    case "stream":
                        //row_Image.Height = new GridLength(0);
                        //row_Video.Height = new GridLength(0);
                        ////row_PDF.Height = new GridLength(0);
                        //row_Stream.Height = GridLength.Auto;
                        //row_Scrittura.Height= new GridLength(0);
                        img_Image.Visibility = Visibility.Hidden;
                        me_Video.Visibility = Visibility.Hidden;
                        gr_Stream.Visibility = Visibility.Visible;                        
                        img_Scrittura.Visibility = Visibility.Hidden;
                        break;

                    case "scrittura":
                        //row_Image.Height = new GridLength(0);
                        //row_Video.Height = new GridLength(0);
                        ////row_PDF.Height = new GridLength(0);
                        //row_Stream.Height = new GridLength(0);
                        //row_Scrittura.Height = GridLength.Auto;
                        img_Image.Visibility = Visibility.Hidden;
                        me_Video.Visibility = Visibility.Hidden;
                        gr_Stream.Visibility = Visibility.Hidden;
                        img_Scrittura.Visibility = Visibility.Visible;                        

                        img_Scrittura.Height = this.ActualHeight;
                        img_Scrittura.Width = this.ActualWidth;
                        break;
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        #region Image

        public void UpdateImage(ImageSource image)
        {
            
            try
            {
                img_Image.Source = image;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        #region Video

        public void UpdateVideo(string PathFile)
        {
            try
            {
                me_Video.Source = new Uri(PathFile);
                seeker = new DispatcherTimer();
                seeker.Interval = TimeSpan.FromMilliseconds(200);
                seeker.Tick += Seeker_Tick;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        public TimeSpan VideoPosition
        {
            get
            {
                return me_Video.Position;
            }
            set
            {
                me_Video.Position = value; 
            }
        }

        private void Seeker_Tick(object sender, EventArgs e)
        {
            try
            {
                //lb_time.Content = String.Format("{0:hh}:{0:mm}:{0:ss}/{1:hh}:{1:mm}:{1:ss}", me_Video.Position, me_Video.NaturalDuration.TimeSpan);
                //sl_time.Value = me_Video.Position.TotalSeconds * 100 / totaltime.TotalSeconds;
                if (me_Video.NaturalDuration.HasTimeSpan)
                {
                    TickTimerEventArgs timerargs = new TickTimerEventArgs();
                    timerargs.TotalTime = totaltime;
                    timerargs.NowTime = me_Video.Position;
                    OnTickTimer(timerargs);
                }


            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        protected virtual void OnTickTimer(TickTimerEventArgs e)
        {
            try
            {
                EventHandler<TickTimerEventArgs> handler = TickTimer;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        protected virtual void OnEndVideo(EndVideoEventArgs e)
        {
            try
            {
                EventHandler< EndVideoEventArgs> handler = EndVideo;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void Play_Video()
        {
            try
            {
                me_Video.Play();
                seeker.Start();
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void Stop_Video()
        {
            try
            {
                me_Video.Stop();
                seeker.Stop();
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        public void Pause_Video()
        {
            try
            {
               me_Video.Pause();
               seeker.Stop();
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void Me_Video_MediaOpened(object sender, RoutedEventArgs e)
        {
            try
            {
                totaltime = me_Video.NaturalDuration.TimeSpan;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Me_Video_MediaEnded(object sender, RoutedEventArgs e)
        {
            try
            {
                EndVideoEventArgs endVideoEvent = new EndVideoEventArgs();
                endVideoEvent.VideoEnd = true;
                OnEndVideo(endVideoEvent);
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion

        //#region PDF

        //public void UpdatePDF(string PathFile)
        //{
        //    try
        //    {
        //        pdf_Viewer.LoadFile(PathFile);
        //    }
        //    catch (Exception ex)
        //    {
        //        string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
        //        MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }

        //}


        //#endregion

        #region Stream

        public void UpdateStream(WebCameraId webcam)
        {
            try
            {
                web_id = webcam;
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }            
        }


        public void StartStream(WebCameraId webCamera)
        {
            try
            {
                we_CameraControl.StartCapture(webCamera);
            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        public void Stopstream()
        {
            try
            {
                if (we_CameraControl.IsCapturing)
                {
                    we_CameraControl.StopCapture();
                }

            }
            catch (Exception ex)
            {
                string exMessage = ExceptionHelper.BuildMessage(mClassName, MethodBase.GetCurrentMethod(), ex);
                MessageBox.Show("Errore: " + exMessage, "Attenzione", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        #endregion


    }

    public class TickTimerEventArgs : EventArgs
    {
        public TimeSpan TotalTime { get; set; }
        public TimeSpan NowTime { get; set; }
    }

    public class EndVideoEventArgs : EventArgs
    {
        public bool VideoEnd { get; set; }
    }
}
