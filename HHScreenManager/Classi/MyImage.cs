﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace HHScreenManager.Classi
{
    public class MyImage
    {
        public string Name { get; set; }
        public BitmapImage ImageData { get; set; }
        public string PathFile { get; set; }
    }
}
