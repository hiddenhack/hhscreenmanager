﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HHScreenManager.Classi
{
    class ExceptionHelper
    {
        private static readonly string mClassName = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name;

        public static string BuildMessage(string className, MethodBase method, Exception ex)
        {
            string trattini = new string('-', 80);
            string separator = new string('-', 10);
            StringBuilder sb = new StringBuilder(string.Empty);
            sb.Append(">>");
            sb.AppendLine(trattini);
            sb.AppendFormat("Classe: {0} Metodo {1}", className, method.Name);
            sb.AppendLine();
            sb.AppendLine(separator);
            if (ex.InnerException != null)
            {
                Exception inner = ex.InnerException;
                Stack<string> messageStack = new Stack<string>();
                do
                {
                    messageStack.Push(string.Format("Tipo eccezione: {0}", inner.GetType()));
                    messageStack.Push(inner.Message);
                    messageStack.Push(string.Empty);
                    if (inner.InnerException == null)
                    {
                        break; // TODO: might not be correct. Was : Exit Do
                    }
                    inner = inner.InnerException;
                } while (true);
                sb.AppendLine(separator);
                sb.AppendLine();
                while (messageStack.Count > 0)
                {
                    sb.AppendLine(messageStack.Pop());
                }
            }
            sb.AppendLine(separator);
            sb.AppendFormat("Eccezione più esterna: {0}", ex.GetType());
            sb.AppendLine();
            sb.AppendLine(separator);
            sb.AppendLine(ex.Message);
            sb.Append("<<");
            return sb.ToString();
        }
    }
}
