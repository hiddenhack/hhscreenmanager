﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsDisplayAPI.DisplayConfig;

namespace HHScreenManager.Classi
{
    public class Monitor
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }

        public System.Windows.Forms.Screen screen { get; set; }
    }

    public class Monitors : List<Monitor>
    {
        public Monitors()
        {

        }

        public static List<Monitor> GetMonitors(bool GetAlsoPrimary)
        {
            List<Monitor> lista = new List<Monitor>();
            try
            {
                foreach (var display in WindowsDisplayAPI.Display.GetDisplays())
                {
                    if (GetAlsoPrimary)
                    {
                        Monitor nm = new Monitor
                        {
                            DisplayName = display.DisplayName,
                            screen = display.GetScreen()
                        };
                        lista.Add(nm);
                    }
                    else
                    {
                        if (!display.IsGDIPrimary)
                        {
                            Monitor nm = new Monitor
                            {
                                DisplayName = display.DisplayName,
                                screen = display.GetScreen()
                            };
                            lista.Add(nm);
                        }
                    }

                }

                foreach (var monitor in lista)
                {
                    monitor.Name = PathDisplayTarget.GetDisplayTargets().Where(x => x.ToDisplayDevice().DisplayName == monitor.DisplayName).FirstOrDefault().FriendlyName;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lista;
        }
    }
}
