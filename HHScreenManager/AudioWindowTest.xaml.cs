﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WebEye.Controls.Wpf;

namespace HHScreenManager
{
    /// <summary>
    /// Logica di interazione per AudioWindowTest.xaml
    /// </summary>
    public partial class AudioWindowTest : Window
    {
        public AudioWindowTest()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer mediaPlayer = new MediaPlayer();            
            
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            WebCameraId wId = weControl.GetVideoCaptureDevices().FirstOrDefault();
            weControl.StartCapture(wId);
            
            

        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            weControl.StopCapture();
        }
    }
}
